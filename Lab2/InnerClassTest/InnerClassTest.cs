namespace ConsoleApp1.InnerClassTest;

public class InnerClassTest {
    private class InnerClass {
        public void InnerMethod() {
            Console.WriteLine("Inner method");
        }
    }

    private InnerClass _innerClass;

    public InnerClassTest() {
        _innerClass = new InnerClass();
    }

    public void callInnerMethod() {
        _innerClass.InnerMethod();
    }
   
}

class Test {
    // public static void Main(String[] args) {
    //     InnerClassTest innerClassTest = new InnerClassTest();
    //     innerClassTest.callInnerMethod();
    // }
}
