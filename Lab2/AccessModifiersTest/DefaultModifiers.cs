namespace ConsoleApp1.AccessModifiersTest;

class DefaultAccessClass
{
    int _defaultField = 10;
    
    struct DefaultStruct
    {
        int x;
        int y;
    }
    
    interface IDefaultInterface
    {
        void Method();
    }
    
    class NestedClass
    {
        void Method()
        {
            // Console.WriteLine(defaultField);
        }
    }
    
    void DefaultMethod()
    {
        Console.WriteLine(_defaultField);
    }
}

class Program
{
    // static void Main(string[] args)
    // {
    //     // Доступ до поля класу DefaultAccessExample
    //     DefaultAccessClass example = new DefaultAccessClass();
    //     // Console.WriteLine(example.defaultField); 
    //     
    //     //DefaultAccessExample.DefaultStruct structInstance = new DefaultAccessExample.DefaultStruct();
    //     //Console.WriteLine(structInstance.x);
    //     
    //     // example.DefaultMethod(); 
    //     
    //     // NestedClass nested = new NestedClass();
    //     //nested.Method();
    // }
}