namespace ConsoleApp1.CallSequence;

public class CallSequenceTest
{
    static int staticField = InitializeStaticField();
    int instanceField = InitializeInstanceField();

    static CallSequenceTest()
    {
        Console.WriteLine("Виклик статисного конструктора");
    }

    public CallSequenceTest()
    {
        Console.WriteLine("Виклик звичайного конструктора");
    }

    static int InitializeStaticField()
    {
        Console.WriteLine("Створення статичної змінної");
        return 0;
    }

    static int InitializeInstanceField()
    {
        Console.WriteLine("Створення звичайної змінної");
        return 0;
    }
}


class Test
{
    // static void Main(string[] args)
    // {
    //     Console.WriteLine("Створення обєкта:");
    //     CallSequenceTest obj1 = new CallSequenceTest();
    // }
}