namespace Lab3_Linq;
using System.Linq;

public class Tests
{
    private static readonly List<Item> Items =
    [
        new Item(1, "Wood", 12),
        new Item(2, "Knife", 3),
        new Item(3, "Stone", 31),
        new Item(4, "Apples", 21),
        new Item(5, "Iron Ore", 67),
        new Item(6, "Seeds", 190),
        new Item(7, "Corn Seeds", 32),
        new Item(8, "Iron Bar", 46),
        new Item(9, "Stone Brick", 123),
        new Item(10, "Pants", 2)
    ];

    private static readonly Dictionary<int, Item> ItemsDictionary = Items.ToDictionary(item => item.Id);

    private static readonly List<Chest> Chests =
    [
        new Chest("Chest1", 1, 20, false),
        new Chest("Chest4", 2, 15, false),
        new Chest("Chest3", 3, 15, false),
        new Chest("Chest5", 4, 30, false),
        new Chest("Chest2", 5, 30, false),
        new Chest("Chest6", 6, 30, false),
        new Chest("Chest7", 3, 50, false),
        new Chest("Chest8", 8, 50, true),
        new Chest("Chest9", 9, 100, true),
        new Chest("Chest10", 10, 100, true)
    ];
    
    private static readonly Dictionary<int, List<Chest>> ChestsDictionary = Chests
        .GroupBy(chest => chest.ItemId)
        .ToDictionary(group => group.Key, group => group
        .ToList());

    [Test]
    public void PrintToConsole()
    {
        Console.WriteLine("Items:");
        
        foreach (var item in Items)
        {
            Console.WriteLine(String.Format("Id: {0}, " +
                                            "Name: {1}, " +
                                            "Amount: {2}", item.Id, item.Name, item.Amount));
        }

        Console.WriteLine("\nItems Dictionary:");
        foreach (var kvp in ItemsDictionary)
        {
            Console.WriteLine(String.Format("Key: {0}, " +
                                            "Value: {1}", kvp.Key, kvp.Value));
        }

        Console.WriteLine("\nChests:");
        foreach (var chest in Chests)
        {
            Console.WriteLine(String.Format("Name: {0}, " +
                                            "ItemId: {1}, " +
                                            "Capacity: {2}, " +
                                            "IsFull: {3}", 
                chest.Name, chest.ItemId, chest.Capacity, chest.IsFull));
        }

        Console.WriteLine("\nChests Dictionary:");
        foreach (var kvp in ChestsDictionary)
        {
            Console.WriteLine(String.Format("Key: {0}," +
                                            " Value: {1}", 
                kvp.Key, string.Join(", ", kvp.Value.Select(c => c.Name))));
        }
    }
    
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void SelectTest()
    {
        //When
        var selectedItems = 
            Items.Take(2).Select(el => new { el.Name, el.Amount });
        var selectedChests =
            Chests.Take(2).Select(ch => new { ch.Name, ch.Capacity });
        var selectedItemsDictionary =
        ItemsDictionary.Select(kv => new { kv.Value.Name, kv.Value.Amount });
        var selectedChestsDictionary =
         ChestsDictionary.SelectMany(kv => kv.Value.Select(ch => new { ch.Name, ch.Capacity }));
        //Then
        Assert.Multiple(() =>
        {
            Assert.That(selectedItems.Count(), Is.EqualTo(2));
            Assert.That(selectedChests.Count(), Is.EqualTo(2));
            Assert.That(selectedItemsDictionary.Count, Is.EqualTo(10));
            Assert.That(selectedChestsDictionary.Count, Is.EqualTo(10));
        });
    }

    [Test]
    public void WhereTest()
    {
        //When
        var selectedChests =  Chests.Where(ch => ch.Capacity >= 30);
        var selectedItems = Items.Where(it => it.Amount > 40);
        var chestsWithFreeSpace = Chests.Where(ch => ch.IsFull);
        var chestsWithCertainItemId = Chests.Where(ch => ch.ItemId == 3);
        
        //Then
        Assert.Multiple(() =>
        {
            Assert.That(selectedChests.Count(), Is.EqualTo(7));
            Assert.That(chestsWithFreeSpace.Count(), Is.EqualTo(3));
            Assert.That(selectedItems.Count(), Is.EqualTo(4));
            Assert.That(chestsWithCertainItemId.Count(), Is.EqualTo(2));
            
        });
    }
    
    
    [Test]
    public void DictionaryWhereTest()
    {
        //When
        var selectedChests = ChestsDictionary
            .Where(kv => kv.Value.Sum(ch => ch.Capacity) >= 60)
            .SelectMany(kv => kv.Value);
        
        var selectedItems = ItemsDictionary
            .Where(kv => kv.Value.Amount > 40)
            .Select(kv => kv.Value);
        
        var chestsWithFreeSpace = ChestsDictionary
            .Where(kv => kv.Value.All(ch => !ch.IsFull))
            .SelectMany(kv => kv.Value);
        
        var chestsWithCertainItemId = ChestsDictionary
            .Where(kv => kv.Value.Any(ch => ch.ItemId == 3))
            .SelectMany(kv => kv.Value);
    
        //Then
        Assert.Multiple(() =>
        {
            Assert.That(selectedChests.Count(), Is.EqualTo(4));
            Assert.That(chestsWithFreeSpace.Count(), Is.EqualTo(7));
            Assert.That(selectedItems.Count(), Is.EqualTo(4));
            Assert.That(chestsWithCertainItemId.Count(), Is.EqualTo(2));
        });
    }

    [Test]
    public void CustomMethodsTest()
    {
        //When 
        var maxAmountItems = Items.MaxAmount();
        var maxChestCapacity = Chests.MaxCapacity();
        var maxAmountItemsDictionary = ItemsDictionary.Values.Max(item => item.Amount);
        var maxChestCapacityDictionary = ChestsDictionary.Values.Max(chests => chests.Sum(ch => ch.Capacity));
        //Then
        Assert.Multiple(() =>
        {
            Assert.That(maxAmountItems, Is.EqualTo(190));
            Assert.That(maxChestCapacity, Is.EqualTo(100));
            Assert.That(maxAmountItemsDictionary, Is.EqualTo(190));
            Assert.That(maxChestCapacityDictionary, Is.EqualTo(100));
        });
    }

    [Test]
    public void AnonymousObjectAndInitializerTest()
    {
        //When
        var anonymousObject = new { Name = "Anonymous Object", Value = 42 };
        //Then
        Assert.Multiple(() =>
        {
            Assert.That(anonymousObject.Name, Is.EqualTo("Anonymous Object"));
            Assert.That(anonymousObject.Value, Is.EqualTo(42));
        });
    }

    [Test]
    public void UsingIComparerTest()
    {
        //When
        var sortedChestsByAmount = Chests
            .OrderBy(chest => chest.Capacity, new CompareCapacity());
        
        var sortedChestsByAmountDictionary = ChestsDictionary.Values
            .OrderBy(chests => chests
            .Sum(ch => ch.Capacity), new CompareCapacity());

        Assert.Multiple(() =>
        {
            //Then
            // ReSharper disable once PossibleMultipleEnumeration
            Assert.That(sortedChestsByAmount.First().Name, Is.EqualTo("Chest4"));
            // ReSharper disable once PossibleMultipleEnumeration
            Assert.That(sortedChestsByAmount.Last().Name, Is.EqualTo("Chest10"));
            
            // ReSharper disable once PossibleMultipleEnumeration
            Assert.That(sortedChestsByAmountDictionary.First().First().Name, Is.EqualTo("Chest4"));
            // ReSharper disable once PossibleMultipleEnumeration
            Assert.That(sortedChestsByAmountDictionary.Last().First().Name, Is.EqualTo("Chest10"));
        });
    }
    
    [Test]
    public void ConvertListToArrayTest()
    {
        //When
        var itemsArray = Items.ToArray();
        var itemsArrayFromDictionary = ItemsDictionary.Values.ToArray();
        //Then
        Assert.That(itemsArray, Has.Length.EqualTo(10));
        Assert.That(itemsArrayFromDictionary, Has.Length.EqualTo(10));
    }
    
    [Test]
    public void SortingArrayByNameOrAmountTest()
    {
        //Given
        var names = new[] { "Apples", "Corn Seeds", "Iron Bar", "Iron Ore", "Knife", "Pants", "Seeds", "Stone", "Stone Brick", "Wood" };
        var amounts = new[] { 2, 3, 12, 21, 31, 32, 46, 67, 123, 190};
        //When
        var sortedItemsByName = Items.OrderBy(item => item.Name).Select(item => item.Name);
        var sortedItemsByAmount = Items.OrderBy(item => item.Amount).Select(item => item.Amount);
        //Then
        Assert.That(sortedItemsByName, Is.EqualTo(names));
        Assert.That(sortedItemsByAmount, Is.EqualTo(amounts));
    }
    
    [Test]
        public void SortedListOperationsTest()
        {
            //Given
            var sortedItems = new SortedList<string, int>();
            foreach (var item in Items)
            {
                sortedItems.Add(item.Name, item.Amount);
            }

            //When
            var firstItem = sortedItems.First();
            var lastItem = sortedItems.Last();
            var containsItem = sortedItems.ContainsKey("Wood");

            //Then
            Assert.Multiple(() =>
            {
                Assert.That(firstItem.Key, Is.EqualTo("Apples"));
                Assert.That(lastItem.Key, Is.EqualTo("Wood"));
                Assert.IsTrue(containsItem);
            });
        }

        [Test]
        public void QueueOperationsTest()
        {
            //Given
            var queue = new Queue<Item>(Items);

            //When
            var dequeuedItem = queue.Dequeue();
            var peekedItem = queue.Peek();
            queue.Enqueue(new Item(11, "New Item", 50));

            //Then
            Assert.Multiple(() =>
            {
                Assert.That(dequeuedItem.Name, Is.EqualTo("Wood"));
                Assert.That(peekedItem.Name, Is.EqualTo("Knife"));
                Assert.That(queue.Count, Is.EqualTo(10));
            });
        }

        [Test]
        public void GroupingAndAdvancedListOperationsTest()
        {
            //When
            var groupedItemsByAmount = Items.GroupBy(item => item.Amount % 10);
            var itemsWithMaxAmount = Items.Where(item => item.Amount == Items.MaxAmount());
            
            //Then
            Assert.Multiple(() =>
            {
                Assert.That(groupedItemsByAmount.Count(), Is.EqualTo(6));
                Assert.That(itemsWithMaxAmount.Count(), Is.EqualTo(1));
            });
        }
}


public static class CustomExtensions
{
    public static int MaxAmount(this IEnumerable<Item> items)
    {
        return items.Max(item => item.Amount);
    }
    
    public static int MaxCapacity(this IEnumerable<Chest> chests)
    {
        return chests.Max(chest => chest.Capacity);
    }
}

public class CompareCapacity : IComparer<int>
{
    public int Compare(int capacityOne, int capacityTwo)
    {
        return capacityOne.CompareTo(capacityTwo);
    }
}


