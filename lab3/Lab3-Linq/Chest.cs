namespace Lab3_Linq;

public class Chest(string name, int itemsId, int capacity, bool isFull)
{
    public string Name { get; set; } = name;
    public  int ItemId { get; set; } = itemsId;

    public int Capacity { get; set; } = capacity;
    public bool IsFull { get; set; } = isFull;
}

public class Item(int id, string name, int amount)
{
    public string Name { get; set; } = name;
    public int Id { get; set; } = id;
    public int Amount { get; set; } = amount;
}