using System.Diagnostics;

namespace ConsoleApp1;

public class ChildClass : ParentClass
{
    public new void Speak()
    {
        Console.WriteLine("Invoked method from child class!");
    }
}

partial class Test
{
    // public static void Main(string[] args)
    // {
    //     const int numberOfObjects = 10000000;
    //     //Створення 10 мільйонів насдідуваних класів
    //     
    //     ChildClass[] childClasses = new ChildClass[numberOfObjects];
    //     
    //     Stopwatch stopwatchChildClasses = Stopwatch.StartNew();
    //     
    //     for (int i = 0; i < numberOfObjects; i++)
    //     {
    //         childClasses[i] = new ChildClass();
    //     }
    //
    //     stopwatchChildClasses.Stop();
    //     
    //     Console.WriteLine("Child classes created successfully.");
    //     Console.WriteLine("Time taken: " + stopwatchChildClasses.ElapsedMilliseconds + " milliseconds.");
    // }
}