namespace ConsoleApp1.MultipleInheritance;

using System;

class BaseClass
{
    public void MethodA()
    {
        Console.WriteLine("Method A from BaseClass");
    }
}

interface Interface1
{
    void MethodB();
}

interface Interface2
{
    void MethodC();
}

class DerivedClass : BaseClass, Interface1, Interface2
{
    public void MethodB()
    {
        Console.WriteLine("Method B from Interface1");
    }

    public void MethodC()
    {
        Console.WriteLine("Method C from Interface2");
    }
}

class Program
{
    // static void Main(string[] args)
    // {
    //     DerivedClass derived = new DerivedClass();
    //     derived.MethodA(); 
    //     derived.MethodB(); 
    //     derived.MethodC(); 
    // }
}
