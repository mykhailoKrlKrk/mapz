namespace ConsoleApp1.ObjectClassTest;

using System;

public class ObjectClass
{
    private int value;
    
    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        
        ObjectClass other = (ObjectClass)obj;
        return this.value == other.value;
    }
    
    public override int GetHashCode()
    {
        return this.value.GetHashCode();
    }
    
    public override string ToString()
    {
        return "This is an instance of ObjectClass with value: " + value;
    }
    
    ~ObjectClass()
    {
       //clear
    }
    
    public new Type GetType()
    {
        return base.GetType();
    }
    
    public new ObjectClass MemberwiseClone()
    {
        return (ObjectClass)base.MemberwiseClone();
    }
    
    public new static bool ReferenceEquals(object obj1, object obj2)
    {
        return object.ReferenceEquals(obj1, obj2);
    }
}