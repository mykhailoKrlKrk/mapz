
namespace ConsoleApp1.EnumeratedTypes;


enum DaysOfWeek
{
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday
}


class EnumeratedTypesTest {
    // public static void Main(string[] args)
    // {
    //     DaysOfWeek daysOfWeekOne = DaysOfWeek.Friday;
    //     DaysOfWeek daysOfWeekTwo = DaysOfWeek.Saturday;
    //
    //     // == 
    //     Console.WriteLine(daysOfWeekOne == daysOfWeekTwo);
    //     
    //     // &&
    //     if (daysOfWeekOne == DaysOfWeek.Friday && daysOfWeekTwo == DaysOfWeek.Saturday)
    //     {
    //         Console.WriteLine("Logical &&");
    //     }
    //
    //     // ||
    //     if (daysOfWeekOne == DaysOfWeek.Friday || daysOfWeekTwo == DaysOfWeek.Monday)
    //     {
    //         Console.WriteLine("Logical ||");
    //     }
    //     
    //     // ^
    //     if (daysOfWeekOne == DaysOfWeek.Friday ^ daysOfWeekTwo == DaysOfWeek.Monday)
    //     {
    //         Console.WriteLine("Logical ^");
    //     }
    // }
}