namespace ConsoleApp1.InheritanceExample;

public abstract class Car : ICar{
    public abstract void Start();
}