using System.Diagnostics;
using System.Reflection;

namespace ConsoleApp1;

public struct Structure
{
    public int value;
    public void MethodTwo()
    {
        Console.WriteLine("Invoked method from structure!");
    }
}

partial class Test
{
    // public static void Main(string[] args)
    // {
    //     const int numberOfObjects = 10000000;
    //     //
    //     // //Створення 10 мільйонів структур
    //     // Structure[] structures = new Structure[numberOfObjects];
    //     //
    //     // Stopwatch stopwatchStr = Stopwatch.StartNew();
    //     //
    //     // for (int i = 0; i < numberOfObjects; i++)
    //     // {
    //     //     structures[i] = new Structure();
    //     // }
    //     //
    //     // stopwatchStr.Stop();
    //     //
    //     // Console.WriteLine("Structures created successfully.");
    //     // Console.WriteLine("Time taken: " + stopwatchStr.ElapsedMilliseconds + " milliseconds.");
    //     //
    //     Structure obj = new Structure();
    //     
    //     Stopwatch stopwatchMethods = Stopwatch.StartNew();
    //     
    //     for (int i = 0; i < numberOfObjects; i++)
    //     {
    //         obj.value += 5;
    //     }
    //     
    //     stopwatchMethods.Stop();
    //     
    //     Console.WriteLine("Time taken to invoke method 10 million times: " + stopwatchMethods.ElapsedMilliseconds + " milliseconds.");
    //
    // }
}