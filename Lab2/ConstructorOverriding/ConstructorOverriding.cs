namespace ConsoleApp1.ConstructorOverriding;
using System;

class BaseClass
{
    private int value;
    
    public BaseClass() {
        value = 0;
        Console.WriteLine("BaseClass constructor without parameters called");
    }
    
    public BaseClass(int value) {
        this.value = value;
        Console.WriteLine("BaseClass constructor with parameter called");
    }
    
    public void Method() {
        Console.WriteLine("Method in BaseClass called");
    }
}

class DerivedClass : BaseClass {
    private int derivedValue;

    public DerivedClass()
    {
        derivedValue = 0;
        Console.WriteLine("DerivedClass constructor without parameters called");
    }

    public DerivedClass(int value) : base(value)
    {
        derivedValue = value;
        Console.WriteLine("DerivedClass constructor with parameter called");
    }
    
    public new void Method()
    {
        Console.WriteLine("Method in DerivedClass called");
    }
}

class Test
{
    // static void Main(string[] args) {
    //     BaseClass baseObj1 = new BaseClass();
    //     BaseClass baseObj2 = new BaseClass(10);
    //     DerivedClass derivedObj1 = new DerivedClass();
    //     DerivedClass derivedObj2 = new DerivedClass(20);
    //
    //     baseObj1.Method();    
    //     baseObj2.Method();    
    //     derivedObj1.Method(); 
    //     derivedObj2.Method(); 
    // }
}
