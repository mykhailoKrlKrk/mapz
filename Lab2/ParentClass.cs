using System.Diagnostics;
using System.Reflection;

namespace ConsoleApp1;

public class ParentClass
{
    public int value;
    public void Speak()
    {
        Console.WriteLine();
    }
}

partial class Test
{
    public static void Main(string[] args)
    {
        const int numberOfObjects = 10000000;
        
        // //Створення 10 мільйонів класів
        // ParentClass[] objects = new ParentClass[numberOfObjects];
        //
        // Stopwatch stopwatch = Stopwatch.StartNew();
        //
        // for (int i = 0; i < numberOfObjects; i++)
        // {
        //     objects[i] = new ParentClass();
        // }
        //
        // stopwatch.Stop();
        //
        // Console.WriteLine("Objects created successfully.");
        // Console.WriteLine("Time taken: " + stopwatch.ElapsedMilliseconds + " milliseconds.");
        //
        //Виклик методу за допомогою рефлексії 10 мільйонів раз
        ParentClass obj = new ParentClass();
        
        Stopwatch stopwatchMethods = Stopwatch.StartNew();
        
        for (int i = 0; i < numberOfObjects; i++)
        {
            obj.value += 5;
        }
        
        stopwatchMethods.Stop();
        
        Console.WriteLine("Time taken to invoke method 10 million times: " + stopwatchMethods.ElapsedMilliseconds + " milliseconds.");
        
    }
}