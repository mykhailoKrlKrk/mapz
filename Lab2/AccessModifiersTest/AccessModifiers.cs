namespace ConsoleApp1.AccessModifiersTest;

public class AccessModifiers {
    public int PublicField = 10;
    
    private int PrivateField = 20;
    
    protected int ProtectedField = 30;
    
    internal int InternalField = 40;
    
    protected internal int ProtectedInternalField = 50;
    
    int DefaultField = 60;
    
    public void PublicMethod()
    {
        Console.WriteLine("Public Method");
    }
    
    private void PrivateMethod()
    {
        Console.WriteLine("Private Method");
    }

    protected void ProtectedMethod()
    {
        Console.WriteLine("Protected Method");
    }
    
    internal void InternalMethod()
    {
        Console.WriteLine("Internal Method");
    }
    
    protected internal void ProtectedInternalMethod()
    {
        Console.WriteLine("Protected Internal Method");
    }
    
    void DefaultMethod()
    {
        Console.WriteLine("Default Method");
    }
}

class ModifiersTest
{
    // static void Main(string[] args)
    // {
    //     AccessModifiers example = new AccessModifiers();
    //     
    //     Console.WriteLine("Public Field: " + example.PublicField);
    //     // Console.WriteLine("Private field: " + example.PrivateField);
    //     // Console.WriteLine("Protected Field: " + ProtectedField);
    //     Console.WriteLine("Internal Field: " + example.InternalField);
    //     Console.WriteLine("Protected Internal Field: " + example.ProtectedInternalField);
    //
    //     example.PublicMethod();
    //     // example.PrivateMethod();
    //     // example.ProtectedMethod();
    //     example.InternalMethod();
    //     example.ProtectedInternalMethod();
    // }
}